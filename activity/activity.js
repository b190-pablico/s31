// 1. What directive is used by Node.js in loading the modules it needs?

const http = require("http");

const port = 4000;

// 2. What Node.js module contains a method for server creation?

const server = http.createServer((request, response) => {

// 3. What is the method of the http object responsible for creating a server using Node.js?

	if (request.url === "/login"){

// 4. What method of the response object allows us to set status codes and content types?
	
		response.writeHead(200, {"Content-Type": "text/plain"});
		response.end("Welcome to login page.");
	} else {
		response.writeHead(404, {"Content-Type": "text/plain"});
		response.end("I'm sorry the page you are looking for cannot be found.");
	}
});

// 5. Where will console.log() output its contents when run in Node.js?

console.log(`Server now running at port: ${port}`);

// 6. What property of the request object contains the address's endpoint?
server.listen(port);